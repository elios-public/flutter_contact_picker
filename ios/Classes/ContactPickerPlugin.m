// Copyright 2017 Michael Goderbauer. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import "ContactPickerPlugin.h"
@import ContactsUI;

@interface ContactPickerPlugin ()<CNContactPickerDelegate>
@end

@implementation ContactPickerPlugin {
  FlutterResult _result;
}

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar> *)registrar {
  FlutterMethodChannel *channel =
      [FlutterMethodChannel methodChannelWithName:@"contact_picker"
                                  binaryMessenger:[registrar messenger]];
  ContactPickerPlugin *instance = [[ContactPickerPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall *)call result:(FlutterResult)result {
    NSArray *myMethods = [NSArray arrayWithObjects: @"selectContact", nil];
    // Check if the method call exists.
    if ([myMethods containsObject:call.method])
    {
        if (_result)
        {
            //Making custom error Flutter reply to method called
            _result([FlutterError errorWithCode:@"multiple_requests"
                                  message:@"Cancelled by a second request."
                                  details:nil]);
            _result = nil;
        }
        
        _result = result;

         CNContactPickerViewController *contactPicker = [[CNContactPickerViewController alloc] init];
        contactPicker.delegate = self;
      contactPicker.displayedPropertyKeys = (NSArray *)CNContactGivenNameKey;
     
                contactPicker.displayedPropertyKeys = @[CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey];
      
        UIViewController *viewController = [UIApplication sharedApplication].delegate.window.rootViewController;
        [viewController presentViewController:contactPicker animated:YES completion:nil];
  }
    else
    {
        result(FlutterMethodNotImplemented);
    }
}

- (void)contactPicker:(CNContactPickerViewController *)picker
    didSelectContacts : (NSArray<CNContact *> *)contacts {

    NSUInteger count;
    NSString *contactPhone = nil;
    NSString *contactEmail = nil;
    NSMutableArray *contactNumberArray = [[NSMutableArray alloc]init];
    
    //Defining variables and setting default values
 
    NSString *contactDictionaryPhoneNumberType = @"phoneNumber";
    NSString *contactDictionaryEmailAddressType = @"emailAddress";
    
    NSDictionary *contactDictionary = nil;
    count = [contacts count];

    for (int i = 0; i < count; i++){

        contactEmail = [contacts objectAtIndex: i].emailAddresses.firstObject.value;
        contactPhone = [contacts objectAtIndex: i].phoneNumbers.firstObject.value.stringValue;
        
        
        NSString *fullName = [CNContactFormatter stringFromContact:[contacts objectAtIndex: i]
                                                             style:CNContactFormatterStyleFullName];
        
        // Save to Dictionary
        contactDictionary = [NSDictionary dictionaryWithObjectsAndKeys: fullName, @"fullName", contactPhone, contactDictionaryPhoneNumberType,contactEmail, contactDictionaryEmailAddressType, nil];
        

        // Save Dictionary info in new created array
        [contactNumberArray addObject:(contactDictionary)] ;
    }
    _result(contactNumberArray);

  _result = nil;
}

- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
  _result(nil);
  _result = nil;
}

@end
