// Copyright 2017 Michael Goderbauer. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:contact_picker/contact_picker.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final ContactPicker _contactPicker = ContactPicker();
  List<Contact> _contact = <Contact>[];

  @override
  Widget build(BuildContext context) => MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Plugin example app'),
          ),
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                MaterialButton(
                  color: Colors.blue,
                  child: const Text("CLICK ME"),
                  onPressed: () async {
                    final List<Contact> contact =
                        await _contactPicker.selectContact();
                    setState(() {
                      _contact = List.from(contact);
                    });
                  },
                ),
                Expanded(
                    child: ListView.builder(
                        itemCount: _contact.length,
                        itemBuilder: (BuildContext context, int index) =>
                            ListTile(
                              title: Text(_contact[index].fullName.toString()),
                              subtitle: Text(_contact[index].emailAddress +
                                  ' ' +
                                  _contact[index].phoneNumber),
                              isThreeLine: true,
                            )))
              ],
            ),
          ),
        ),
      );
}
