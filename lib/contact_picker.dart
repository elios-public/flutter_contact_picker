// Copyright 2017 Michael Goderbauer. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';

import 'package:flutter/services.dart';

/// Entry point for the ContactPicker plugin.
///
/// Call [selectContact] to bring up a dialog where the user can pick a contact
/// from his/her address book.
class ContactPicker {
  ContactPicker();

  static const MethodChannel _channel = const MethodChannel('contact_picker');

  /// Brings up a dialog where the user can select a contact from his/her
  /// address book.
  ///
  /// Returns the [Contact] selected by the user, or `null` if the user canceled
  /// out of the dialog.
  Future<List<Contact?>> selectContact() async {
    final List<Contact> contacts = <Contact>[];
    final dynamic result =
        await _channel.invokeMethod<dynamic>('selectContact');

    if (result != null) {
      result.forEach((dynamic contact) {
        final newContact = Contact.fromMap(contact);
        contacts.add(newContact);
      });
    }
    return contacts;
  }
}

/// Represents a contact selected by the user.
class Contact {
  Contact({required this.fullName, this.phoneNumber, this.emailAddress});

  factory Contact.fromMap(Map<dynamic, dynamic> map) => Contact(
      fullName: map['fullName'] ?? '',
      phoneNumber: map['phoneNumber'] ?? '',
      emailAddress: map['emailAddress'] ?? '');

  /// The full name of the contact, e.g. "Dr. Daniel Higgens Jr.".
  final String fullName;

  /// The phone number of the contact.
  final String? phoneNumber;

  final String? emailAddress;

  @override
  String toString() => '$fullName -- Phone: $phoneNumber  Email: $emailAddress';
}
